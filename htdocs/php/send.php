<?php

$email = urldecode(stripslashes($_REQUEST['email']));
$name = urldecode(stripslashes($_REQUEST['name']));

// Mail pour l'admin
$admin = 'michel.somato@free.fr';

// Sujet
$subject = 'Nouvel demande de renseignement';

// message
$message = "
    <html>
    <head>
    <title>Nouvel demande de renseignement</title>
    </head>
    <body>
    <p>Nom: $name, Email: $email</p>
    </body>
    </html>";

// Pour envoyer un mail HTML, l'entêtes Content-type doit être défini
$headers = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=utf8' . "\r\n";

// Entêtes additionnelles
//$headers .= 'To: Alain <alain.vitry@gmail.com>' . "\r\n";
$headers .= 'From: Michel Tiok-Hong <michel.somato@free.fr>' . "\r\n";

// Envoi
mail($admin, $subject, $message, $headers);

// Mail pour l'utilisateur
// Sujet
$subject = 'Michel Tiok-Hong, somatopathe - votre message est pris en compte';

// message
$message = "
    <html>
    <head>
    <title>Bonjour</title>
    </head>
    <body>
        <p>Bonjour,</p>
        <p>Votre demande d'information a bien été prise en compte.</p>
        <p>Je vous recontacterai dans les meilleurs délais.
        <p>Merci</p>
        <p>Michel Tiok-Hong</p>
    </body>
    </html>
";

// Pour envoyer un mail HTML, l'entête Content-type doit être défini
$headers = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=utf8' . "\r\n";

// En-t\u00eates additionnels
//$headers .= "To:  <$email>\r\n";
$headers .= "From: Michel Tiok-Hong <michel.somato@free.fr>\r\n";
mail($email, $subject, $message, $headers);

// Response output
header("Content-Type: application/json");
echo json_encode(array('create' => "ok"));
?>
